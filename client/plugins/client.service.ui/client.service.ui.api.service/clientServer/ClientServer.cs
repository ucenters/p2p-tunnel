﻿using client.service.ui.api.clientServer;
using common.libs;
using common.libs.extends;
using Fleck;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace client.service.ui.api.service.clientServer
{
    public class ClientServer : IClientServer
    {
        private readonly ConcurrentDictionary<Guid, IWebSocketConnection> websockets = new();

        private readonly Dictionary<string, PluginPathCacheInfo> plugins = new();
        private readonly Dictionary<string, IClientConfigure> settingPlugins = new();

        private readonly Config config;
        private readonly ServiceProvider serviceProvider;

        public ClientServer(Config config, ServiceProvider serviceProvider)
        {
            this.config = config;
            this.serviceProvider = serviceProvider;
        }

        public void LoadPlugins(Assembly[] assemblys)
        {
            Type voidType = typeof(void);

            IEnumerable<Type> types = assemblys.SelectMany(c => c.GetTypes());
            foreach (Type item in types.Where(c => c.GetInterfaces().Contains(typeof(IClientService))))
            {
                string path = item.Name.Replace("ClientService", "");
                object obj = serviceProvider.GetService(item);
                foreach (MethodInfo method in item.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly))
                {
                    string key = $"{path}/{method.Name}".ToLower();
                    if (!plugins.ContainsKey(key))
                    {
                        plugins.TryAdd(key, new PluginPathCacheInfo
                        {
                            IsVoid = method.ReturnType == voidType,
                            Method = method,
                            Target = obj,
                            IsTask = method.ReturnType.GetProperty("IsCompleted") != null && method.ReturnType.GetMethod("GetAwaiter") != null,
                            IsTaskResult = method.ReturnType.GetProperty("Result") != null
                        });
                    }
                }
            }
            foreach (Type item in types.Where(c => c.GetInterfaces().Contains(typeof(IClientConfigure))))
            {
                if (!settingPlugins.ContainsKey(item.Name))
                    settingPlugins.Add(item.Name, (IClientConfigure)serviceProvider.GetService(item));
            }
        }
        public void Start()
        {
            WebSocketServer server = new($"ws://{config.Websocket.BindIp}:{config.Websocket.Port}");
            server.RestartAfterListenError = true;
            FleckLog.LogAction = (level, message, ex) =>
            {
                switch (level)
                {
                    case LogLevel.Debug:
                        //Logger.Instance.Info(message);
                        break;
                    case LogLevel.Info:
                        //Logger.Instance.Info(message);
                        break;
                    case LogLevel.Warn:
                        Logger.Instance.Debug(message);
                        break;
                    case LogLevel.Error:
                        Logger.Instance.Error(message);
                        break;
                    default:
                        break;
                }
            };

            server.Start(socket =>
            {
                socket.OnClose = () =>
                {
                    websockets.TryRemove(socket.ConnectionInfo.Id, out _);
                };
                socket.OnOpen = () =>
                {
                    websockets.TryAdd(socket.ConnectionInfo.Id, socket);
                };
                socket.OnMessage = message =>
                {
                    Task.Run(async () =>
                    {
                        await socket.Send((await OnMessage(message.DeJson<ClientServiceRequestInfo>(), socket).ConfigureAwait(false)).ToJson());
                    });
                };
            });
        }

        public IClientConfigure GetConfigure(string className)
        {
            settingPlugins.TryGetValue(className, out IClientConfigure plugin);
            return plugin;
        }
        public IEnumerable<ClientServiceConfigureInfo> GetConfigures()
        {
            return settingPlugins.Select(c => new ClientServiceConfigureInfo
            {
                Name = c.Value.Name,
                Author = c.Value.Author,
                Desc = c.Value.Desc,
                ClassName = c.Value.GetType().Name,
                Enable = c.Value.Enable
            });
        }
        public IEnumerable<string> GetServices()
        {
            return plugins.Select(c => c.Value.Target.GetType().Name).Distinct();
        }

        public void Notify(ClientServiceResponseInfo resp)
        {
            string msg = resp.ToJson();
            foreach (var item in websockets.Values)
            {
                item.Send(msg);
            }
        }

        public async Task<ClientServiceResponseInfo> OnMessage(ClientServiceRequestInfo model, IWebSocketConnection connection)
        {
            model.Path = model.Path.ToLower();
            if (!plugins.ContainsKey(model.Path))
            {
                return new ClientServiceResponseInfo
                {
                    Content = "不存在这个路径",
                    RequestId = model.RequestId,
                    Path = model.Path,
                    Code = -1
                };
            }

            PluginPathCacheInfo plugin = plugins[model.Path];
            try
            {
                ClientServiceParamsInfo param = new ClientServiceParamsInfo
                {
                    RequestId = model.RequestId,
                    Content = model.Content,
                    Path = model.Path,
                    Connection = connection
                };
                dynamic resultAsync = plugin.Method.Invoke(plugin.Target, new object[] { param });
                object resultObject = null;
                if (!plugin.IsVoid)
                {
                    if (plugin.IsTask)
                    {
                        await resultAsync.ConfigureAwait(false);
                        if (plugin.IsTaskResult)
                        {
                            resultObject = resultAsync.Result;
                        }
                    }
                    else
                    {
                        resultObject = resultAsync;
                    }
                }
                return new ClientServiceResponseInfo
                {
                    Content = param.Code == 0 ? resultObject : param.ErrorMessage,
                    RequestId = param.RequestId,
                    Path = param.Path,
                    Code = param.Code
                };
            }
            catch (Exception ex)
            {
                Logger.Instance.Error(ex);
                return new ClientServiceResponseInfo
                {
                    Content = ex.Message,
                    RequestId = model.RequestId,
                    Path = model.Path,
                    Code = -1
                };
            }
        }
        //private const string pipeName = "client.cmd";
        //private void NamedPipe()
        //{
        //    Pipeline pipeline = new Pipeline(pipeName);
        //    Task.Run(async () =>
        //    {
        //        await pipeline.Server.WaitForConnectionAsync().ConfigureAwait(false);
        //        NamedPipe();

        //        while (true)
        //        {
        //            try
        //            {
        //                string msg = await pipeline.Reader.ReadLineAsync().ConfigureAwait(false);
        //                if (string.IsNullOrWhiteSpace(msg))
        //                {
        //                    pipeline.Dispose();
        //                    break;
        //                }
        //                ClientServiceResponseInfo result = await OnMessage(msg.DeJson<ClientServiceRequestInfo>()).ConfigureAwait(false);
        //                await pipeline.Writer.WriteLineAsync(result.ToJson()).ConfigureAwait(false);
        //            }
        //            catch (Exception)
        //            {
        //                pipeline.Dispose();
        //                break;
        //            }
        //        }
        //    });
        //}
    }


    public struct PluginPathCacheInfo
    {
        public object Target { get; set; }
        public MethodInfo Method { get; set; }
        public bool IsVoid { get; set; }
        public bool IsTask { get; set; }
        public bool IsTaskResult { get; set; }
    }

    //public class Pipeline
    //{
    //    public NamedPipeServerStream Server { get; private set; }
    //    public StreamWriter Writer { get; private set; }
    //    public StreamReader Reader { get; private set; }

    //    public Pipeline(string pipeName)
    //    {
    //        Server = new NamedPipeServerStream(pipeName, PipeDirection.InOut, 254, PipeTransmissionMode.Byte, PipeOptions.Asynchronous);
    //        Writer = new StreamWriter(Server);
    //        Reader = new StreamReader(Server);
    //    }
    //    public void Dispose()
    //    {
    //        Server.Close();
    //        Server.Dispose();
    //        Server = null;

    //        Reader.Close();
    //        Reader.Dispose();
    //        Reader = null;

    //        Writer.Close();
    //        Writer.Dispose();
    //        Writer = null;
    //    }
    //}
}
