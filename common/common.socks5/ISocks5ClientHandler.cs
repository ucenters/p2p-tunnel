﻿using System;

namespace common.socks5
{
    public interface ISocks5ClientHandler
    {
        Socks5EnumAuthType HandleRequest(ulong id, Memory<byte> buffer);
        Socks5EnumAuthState HandleAuth(ulong id, Memory<byte> buffer);
        Socks5EnumResponseCommand HandleCommand(ulong id, Memory<byte> buffer);
        void HndleForward(ulong id, Memory<byte> buffer);
        void Close(ulong id);

        void Flush();
    }
}
