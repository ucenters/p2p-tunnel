﻿using common.server;

namespace common.socks5
{
    public class Socks5Messenger : IMessenger
    {
        private readonly Socks5ClientListener socks5ClientListener;
        private readonly ISocks5ServerHandler socks5ServerHandler;
        

        public Socks5Messenger(Socks5ClientListener socks5ClientListener, ISocks5ServerHandler socks5ServerHandler)
        {
            this.socks5ClientListener = socks5ClientListener;
            this.socks5ServerHandler = socks5ServerHandler;
        }
        public void Response(IConnection connection)
        {
            Socks5Info socks5Info = new Socks5Info();
            socks5Info.DeBytes(connection.ReceiveRequestWrap.Memory);
            socks5ClientListener.Response(socks5Info.Id, socks5Info.Data);
        }

        public byte[] Request(IConnection connection)
        {
            Socks5Info socks5Info = new Socks5Info();
            socks5Info.DeBytes(connection.ReceiveRequestWrap.Memory);
            return new byte[] { (byte)socks5ServerHandler.HandleRequest(socks5Info) };
        }

        public byte[] Auth(IConnection connection)
        {
            Socks5Info socks5Info = new Socks5Info();
            socks5Info.DeBytes(connection.ReceiveRequestWrap.Memory);
            return new byte[] { (byte)socks5ServerHandler.HandleAuth(socks5Info) };
        }

        public byte[] Command(IConnection connection)
        {
            Socks5Info socks5Info = new Socks5Info();
            socks5Info.DeBytes(connection.ReceiveRequestWrap.Memory);
            return new byte[] { (byte)socks5ServerHandler.HandleCommand(connection, socks5Info) };
        }

        public void Forward(IConnection connection)
        {
            Socks5Info socks5Info = new Socks5Info();
            socks5Info.DeBytes(connection.ReceiveRequestWrap.Memory);
            socks5ServerHandler.HndleForward(connection,socks5Info);
        }
    }
}
